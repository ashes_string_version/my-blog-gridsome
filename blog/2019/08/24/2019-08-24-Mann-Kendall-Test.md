---
title: Mann-Kendall Test
path: mann-kendall-test
date: 2019-08-24
summary: 'Mann-Kendall test เป็นวิธีทดสอบสมมติฐานแบบไม่อิงพารามิเตอร์'
tags: ['stats']
---

## Mann-Kendall Test

Mann-Kendall Test หรือ MK Test เป็นวิธีทดสอบสมมติฐานแบบไม่อิงพารามิเตอร์(non-parametric) ใช้สำหรับทดสอบว่ามี monotonic upward/downward trend หรือไม่

ตัวอย่างการใช้ MK test เช่น ในด้านของภูมิอากาศ(climate) ต้องการทดสอบว่า อุณหภูมิเฉลี่ยตั้งแต่ปี xxxx ถึง ปี yyyy มี trend ที่เพิ่มขึ้นหรือลดลงหรือไม่

## หลักการของ MK test

หลักการคร่าวๆของ MK test คือ ทำการจับคู่ลบกันแบบพบกันหมด `x ตัวที่เกิดหลัง ลบด้วย x ตัวที่เกิดก่อน` นั่นทำให้ต้องทำการลบทั้งหมด `n(n-1)/2` ครั้ง

ขั้นตอนหลักๆ ของ MK test มีดังนี้

- หาค่า S
- หาค่า variance
- หาค่า z

### หาค่า Mann-Kendall Statistic (S)

$$
 E[S] = S = \sum_{i=1}^{n-1}\sum_{j=i+1}^{n} sign(x_j- x_i)
$$

$x_j$ คือ ตัวที่เกิดขึ้นทีหลัง $x_i$ <br>
$n$ คือ จำนวนตัวอย่าง

$$
sign(x) = \begin{cases}
    1 & \quad \text{if} \quad x > 0 \\
    0 & \quad \text{if} \quad x = 0 \\
    -1 & \quad \text{if} \quad x < 0
\end{cases}
$$

ฟังก์ชัน sign เป็นการแปลงผลของการลบกันให้เป็นตัวเลข 3 ตัวคือ 1, 0 และ -1

### หาค่า variance ของ S

$$
Var(S) = \frac{1}{18}[n(n-1)(2n+5) - \sum_{k=1}^{p}q_k(q_k-1)(2q_k+5)]
$$

p คือ จำนวนของ tie (หรือกลุ่มข้อมูล) <br>
$q_k$ คือ ความถี่ของข้อมูลกลุ่มที่ k

เช่น ถ้ามีข้อมูล

```markdown
[22.5, 22.6, 23.8, 22.5, 23.2, 23.4, 23.2, 23.2]
n = 8
```

ties และ q จะเป็นดังนี้

```markdown
22.5 -> q = 2 ....22.5 ความถี่ที่พบ 2 ครั้ง
22.6 -> q = 1
23.8 -> q = 1
23.2 -> q = 3
23.4 -> q = 1
```

### หาค่า Z ของ S

$$
Z_{MK} = \begin{cases}
    \displaystyle\frac{E[S] - 1}{\sqrt{Var(S)}} & \quad \text{if} \quad E[S] > 0\\
    \\0 & \quad \text{if} \quad E[S] = 0 \\
    \\ \displaystyle\frac{E[S] + 1}{\sqrt{Var(S)}} & \quad \text{if} \quad E[S] < 0\\
\end{cases}
$$

### ทดสอบสมมติฐาน

ทำการหาค่า p-value

$$
\text{p-value} = f(z) = \frac{1}{\sqrt{2 \pi}} e^{-{\frac{z^2}{2}}}
$$

เมื่อทำการหาได้แล้ว ก็นำค่า p-value ไปเทียบกับ significance level

## References:

- [python mkt](https://up-rs-esp.github.io/mkt/)
- [statistic howto mkt](https://www.statisticshowto.datasciencecentral.com/mann-kendall-trend-test/)
- [mkt excel example](https://www.real-statistics.com/time-series-analysis/time-series-miscellaneous/mann-kendall-test/)
- [mkt for monotonic trend](https://vsp.pnnl.gov/help/Vsample/Design_Trend_Mann_Kendall.htm)
- [mkt (pdf)](https://www.statisticshowto.datasciencecentral.com/wp-content/uploads/2016/08/Mann-Kendall-Analysis-1.pdf)

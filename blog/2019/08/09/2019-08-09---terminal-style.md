---
title: Terminal style codeblock
path: terminal-style-codeblock
date: 2019-08-09
summary: 'แก้ไข css ให้ codeblock แสดงผลคล้ายๆหน้าต่าง terminal'
tags: ['blog']
---

บทความนี้จะมาสอนแก้ไข CSS ของ Syntax highlighter ให้เป็นหน้าตาแบบนี้กันครับ
(ตัวอย่างจากเว็ป [carbon](https://carbon.now.sh))
![carbon](carbon.png)

ผลลัพธ์ที่ได้ก็จะได้เป็นแบบนี้

```python
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
sns.set_style("whitegrid")
paired_color = sns.color_palette("Paired")
sns.set_palette("Paired")
sns.set_context('notebook', font_scale=1, rc={"lines.linewidth": 3})

df = pd.read_csv('./data/1951-2019.csv')
df['DATE'] = pd.to_datetime(df['DATE'])
```

เริ่มต้นจากต้องหาไฟล์ css ของ syntax highlighter ที่เราใช้ก่อน โดยของ blog นี้จะอยู่ที่ไฟล์
`src/templates/Post.vue`

ให้เราหาส่วนที่เป็น class `pre` เฉยๆ

```css
.markdown-body .highlight pre,
.markdown-body pre {
  /* ... */
}

/* หรือถ้าหากใช้ prism.js ก็จะเป็น */
pre[class*='language-'] {
  /* ... */
}
```

ให้เปลี่ยนหรือเพิ่มค่าเป็นดังนี้ครับ

```css
.markdown-body .highlight pre,
.markdown-body pre {
  /* ... */
  position: relative;
  padding: 3em 1.5em 1.5em;
}
```

จากนั้นจะทำการเพิ่มวงกลมๆ ทั้งสามวงขึ้นมาโดยทำการเพิ่ม code ดังนี้

```css
.markdown-body pre code:before {
  position: absolute;
  top: 16px;
  left: 18px;
  content: '';
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background-color: #ff766f;
  box-shadow: 18px 0 0 0 #ffc039, 36px 0 0 0 #3bc750;
}

/* หรือถ้าหากใช้ prism.js ก็จะเป็น */
pre[class*='language-'] code:before {
  position: absolute;
  top: 16px;
  left: 18px;
  content: '';
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background-color: #ff5f56;
  box-shadow: 18px 0 0 0 #ffbd2e, 36px 0 0 0 #27c93f;
}
```

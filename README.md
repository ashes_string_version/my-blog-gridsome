# My Gridsome Website

## Notes

- Based on [gridsome-portfolio-starter](https://github.com/drehimself/gridsome-portfolio-starter.git). I wanted to change the primary color and content in the website.

## build

```
gridsome build
```

## build directory

```
dist
```

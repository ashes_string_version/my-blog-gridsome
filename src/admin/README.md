# Netlify CMS config

[guide by gridsome](https://gridsome.org/docs/guide-netlify-cms/)

Use this command to install dependencies

```
npm add netlify-cms gridsome-plugin-netlify-cms
```


Local git
```
npx netlify-cms-proxy-server
```

[Folder Collection](https://www.netlifycms.org/docs/beta-features/)

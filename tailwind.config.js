module.exports = {
  theme: {
    extend: {
      spacing: {
        '80': '20rem',
        '108': '27rem',
      },
      borderWidth: {
        '14': '14px',
      }
    },
    container: {
      padding: '1rem'
    },
    colors: {
      background: {
        primary: 'var(--bg-background-primary)',
        secondary: 'var(--bg-background-secondary)',
        tertiary: 'var(--bg-background-tertiary)',

        form: 'var(--bg-background-form)',
      },

      copy: {
        primary: 'var(--text-copy-primary)',
        secondary: 'var(--text-copy-hover)',
      },

      'border-color': {
        primary: 'var(--border-border-color-primary)',
      },

      transparent: 'transparent',

      black: '#000',
      white: '#fff',

      green: {
        100: '#f0fff4',
        200: '#c6f6d5',
        300: '#9ae6b4',
        400: '#68d391',
        500: '#48bb78',
        600: '#38a169',
        700: '#2f855a',
        800: '#276749',
        900: '#22543d',
      },
      // Color from Ant design
      // blue 1 to blue 9
      // https://ant.design/docs/spec/colors
      blue: {
        100: '#e6f7ff',
        200: '#bae7ff',
        300: '#91d5ff',
        400: '#69c0ff',
        500: '#40a9ff',
        600: '#1890ff',
        700: '#096dd9',
        800: '#0050b3',
        900: '#003a8c',
      },

      cyan: {
        100: '#e6fffb',
        200: '#b5f5ec',
        300: '#87e8de',
        400: '#5cdbd3',
        500: '#36cfc9',
        600: '#13c2c2',
        700: '#08979c',
        800: '#006d75',
        900: '#00474f',
      },

      red: {
        100: "#fff1f0",
        200: "#ffccc7",
        300: "#ffa39e",
        400: "#ff7875",
        500: "#ff4d4f",
        600: "#f5222d",
        700: "#cf1322",
        800: "#a8071a",
        900: "#820014"
        },

      magenta:{
        100: "#fff0f6",
        200: "#ffd6e7",
        300: "#ffadd2",
        400: "#ff85c0",
        500: "#f759ab",
        600: "#eb2f96",
        700: "#c41d7f",
        800: "#9e1068",
        900: "#780650"
      },

      gray: {
        100: '#f7fafc',
        200: '#edf2f7',
        300: '#e2e8f0',
        400: '#cbd5e0',
        500: '#a0aec0',
        600: '#718096',
        700: '#4a5568',
        800: '#2d3748',
        900: '#1a202c',
      },
    },
    fontFamily: {
      sans: [
        'Nunito Sans',
        'Prompt',
        'Roboto',
        '-apple-system',
        '"Segoe UI"',
        '"Helvetica Neue"',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: [
        'Georgia',
        'Pridi',
        'Cambria',
        '"Times New Roman"',
        'Times',
        'serif'
      ],
      mono: [
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace',
      ]
    },
  },
  variants: {
    // Some useful comment
  },
  plugins: [
    // Some useful comment
  ]
}
